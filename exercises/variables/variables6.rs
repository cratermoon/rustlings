// variables6.rs
//
// Execute `rustlings hint variables6` or use the `hint` watch subcommand for a
// hint.



const NUMBER: u64 = 3;
fn main() {
    try_me();
    println!("Number {}", NUMBER);
    const NUMBER: u64 = 7;
    println!("Number {}", NUMBER);
}

fn try_me() {
    println!("Number {}", NUMBER);
}