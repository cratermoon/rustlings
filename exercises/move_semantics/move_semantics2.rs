// move_semantics2.rs
//
// Make the test pass by finding a way to keep both Vecs separate!
//
// Execute `rustlings hint move_semantics2` or use the `hint` watch subcommand
// for a hint.

// I AM NOW DONE

#[test]
fn main() {
    let vec0 = vec![22, 44, 66];

    // here we explicitly clone vec0
    // and the method signature makes it mutable
    let vec1 = fill_vec(vec0.clone());
    // we could also borrow vec0
    // but we then need to clone it
    // in the method
    let vec2 = fill_vec2(&vec0);

    assert_eq!(vec0, vec![22, 44, 66]);
    assert_eq!(vec1, vec![22, 44, 66, 88]);
    assert_eq!(vec2, vec![22, 44, 66, 88]);
}

// vec is mutable
fn fill_vec(mut vec: Vec<i32>) -> Vec<i32> {
    vec.push(88);
    vec
}

// vec is a borrowed reference, it
// is not mutable
fn fill_vec2(vec: &Vec<i32>) -> Vec<i32> {
    let mut v = vec.clone();
    v.push(88);
    v
}
